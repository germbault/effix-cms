import { CustomerModel, UserModel } from "common";

declare global {
    module Express {
        interface Request {
            customer: CustomerModel;
        }
        interface User extends UserModel { }
    }
}
