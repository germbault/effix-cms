import { CustomerModel } from 'common';
import { Router } from 'express';
import { CustomerDAO } from '../dao/customerdao';
import { wrap } from '../util';

const customerRouter = Router();
const customerDAO = new CustomerDAO;


/**
 * @swagger
 * /customer/pubRandom:
 *    get:
 *       tags:
 *       - Client (publicité)
 *       summary: Envoie une publicité aléatoirement.
 *       consumes:
 *       - application/json
 *       responses:
 *         200:
 *          description: Succès
 */

customerRouter.get('/pubRandom', wrap(async (_req, res) => {
    const customers = await customerDAO.getRamdomCustomer();
    return res.send(customers);
}));


customerRouter.use('/:customerId', wrap(async (req, res, next) => {
    const customer = await customerDAO.getCustomer(parseInt(req.params.customerId));
    if (customer === null) { return res.sendStatus(404); }
    req.customer = customer;

    return next();
}));


/**
 * @swagger
 * /customer/:
 *    get:
 *       tags:
 *       - Client (publicité)
 *       summary: Afficher tous les clients.
 *       responses:
 *         200:
 *          description: Succès
 */

customerRouter.get('/', wrap(async (_req, res) => {
    const customers = await customerDAO.getCustomers();
    return res.send(customers);
}));


/**
 * @swagger
 * /customer/{customerId}:
 *    get:
 *       tags:
 *       - Client (publicité)
 *       summary: Afficher un client.
 *       produces:
 *       - application/json
 *       parameters:
 *       - name: customerId
 *         description: Entrer un ID d'un client.
 *         required: true
 *         in: path
 *         type: number
 *       responses:
 *         200:
 *          description: Succès
 */

customerRouter.get('/:customerId', wrap(async (req, res) => {
    return res.send(req.customer);
}));


/**
 * @swagger
 * /customer/:
 *    post:
 *       tags:
 *       - Client (publicité)
 *       summary: Ajouter un client.
 *       produces:
 *       - application/json
 *       parameters:
 *       - in: body
 *         name: body
 *         description: Ajout d'un objet client à la base de donnée.
 *         required: true
 *         schema:
 *           type: object
 *           required:
 *           - userId_Wordpress
 *           - username
 *           properties:
 *             customerId:
 *               type: number
 *             userId_wordpress:
 *               type: number
 *             username:
 *               type: string
 *               example:  Prenom nomfamille
 *             customer_site:
 *               type: string
 *               example: http://www.siteduclient.com
 *             text:
 *               type: string
 *               example:  Entrée ici un court texte.
 *             image:
 *               type: string
 *             publicityActive:
 *               type: string
 *               description: Activé ou désactivé la publicité.
 *               enum:
 *               - activer
 *               - desactiver
 *       responses:
 *         200:
 *          description: Succès
 */

customerRouter.post('/', wrap(async (req, res) => {
    const customer = CustomerModel.fromJSON(req.body);
    const customerId = await customerDAO.createdCustomer(customer);
    return res.send(await customerDAO.getCustomer(customerId));
}));


/**
 * @swagger
 * /customer/{customerId}:
 *    put:
 *       tags:
 *       - Client (publicité)
 *       summary: Modifier la publicité d'un client.
 *       produces:
 *       - application/json
 *       parameters:
 *       - name: customerId
 *         description: Entrer un ID d'un client.
 *         required: true
 *         in: path
 *         type: number
 *       - in: body
 *         name: body
 *         description: Objet client modifiant la publicité de ce dernier.
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             customer_site:
 *               type: string
 *               example: http://www.siteduclient.com
 *             text:
 *               type: string
 *               example: Entrée ici un court texte.
 *             image:
 *               type: string
 *             publicityActive:
 *               type: string
 *               description: Activé ou désactivé la publicité.
 *               enum:
 *               - activer
 *               - desactiver
 *       responses:
 *         200:
 *          description: Succès
 */

customerRouter.put('/:customerId', wrap(async (req, res) => {
    const updated = CustomerModel.fromJSON(req.body);
    updated.customerId = req.customer.customerId;

    await customerDAO.updateCustomer(updated);

    return res.send(await customerDAO.getCustomer(req.customer.customerId));
}));

export { customerRouter };
