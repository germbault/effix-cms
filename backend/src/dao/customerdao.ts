import { CustomerModel } from 'common';
import { DBProvider } from '../dbprovider';

export class CustomerDAO {
    private knex = DBProvider.getKnexConnection();

    public async createdCustomer(customer: CustomerModel) {
        const { userId_wordpress, username, customer_site, text, image, publicityActive } = customer;
        const [customerId] = await this.knex('customer').insert({
            userId_wordpress, username, customer_site, text, image, publicityActive
        });
        return customerId;
    }

    public async getRamdomCustomer() {
        const customer = await this.knex('customer').select('*').where({ publicityActive: 'activer' }).orderByRaw('RAND()').limit(1);
        if (!customer) { return null; }

        return CustomerModel.fromJSON(customer[0] as any);
    }

    public async getCustomer(customerId: number | string) {
        const customer = await this.knex('customer').first('*').where({ customerId });
        if (!customer) { return null; }
        return CustomerModel.fromJSON(customer);
    }

    public async getCustomers() {
        const customers = await this.knex('customer').select('*');

        return customers.map(CustomerModel.fromJSON);
    }
    public async updateCustomer(customer: CustomerModel) {
        const { customerId, username, customer_site, text, image, publicityActive } = customer;
        await this.knex('customer').update({ username, customer_site, text, image, publicityActive }).where({ customerId });
    }
}
