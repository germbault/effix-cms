import bodyParser from 'body-parser';
import { UserModel } from 'common';
import errorHandler from 'errorhandler';
import express from 'express';
import expressFileUpload from 'express-fileupload';
import MySQLStore from 'express-mysql-session';
import session from 'express-session';
import passport from 'passport';
import { Strategy } from 'passport-local';
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import { config } from './config';
import { AuthDAO } from './dao/authdao';
import { authRouter, loginHangler } from './router/authrouter';
import { customerRouter } from './router/customerrouter';
// import { uploadRouter } from './router/uploadrouter';


const authDAO = new AuthDAO;

const sessionStore = new (MySQLStore(session as any))({
    host: config.database.url,
    user: config.database.username,
    password: config.database.password,
    database: config.database.database + '_session'
});

const app = express();

app.set('trust proxy', 'loopback');

const swaggerOptionsCustomer = {
    swaggerDefinition: {
        info: {
            title: 'API client et authentification', // required
            version: '1.0.0',      // required
            description: 'Information sur l\'API relier au client et à l\'authentification.',
            contacts: {
                name: 'Développeur Germain Thibault & Eric Bergeron'
            },
            servers: ['http://localhost:1280']
        }
    },
    
    apis: ['./src/router/*.ts']
};
const swaggerDocsCustomer = swaggerJsDoc(swaggerOptionsCustomer);


app.get('/test', (_req, res) => {
    res.status(200).send('Success Api');
});

app.use(session({
    name: 'effix_session',
    secret: 'oIA6VpPDtfSYNh2AX*r',
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(errorHandler({ log: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use((_req, res, next) => {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    res.header('Access-Control-Allow-Origin', '*');
    next();
});
app.use(expressFileUpload());

passport.serializeUser((user: UserModel, done) => {
    done(null, user.userId);
});

passport.deserializeUser(async (userId: number, done) => {
    const user = await authDAO.getUserById(userId);
    delete user!.password;
    done(null, user ? UserModel.fromJSON(user) : undefined);
});

passport.use(new Strategy(loginHangler));

app.use('/auth', authRouter);
app.use('/customer', customerRouter);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocsCustomer));

// app.use('/upload', uploadRouter);

export { app };
