export class WpModel {
    public id: number;
    public name: string;

    public static fromJSON(jsonWpModel: WpModel) {
        const wpModel = new WpModel;
        Object.assign(wpModel, jsonWpModel);
        return wpModel;
    }
}
