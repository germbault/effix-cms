import { UserContextComponent } from 'context/usercontext';
import { Router } from 'data/router';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
    <UserContextComponent>
        < Router />,
    </UserContextComponent>,
    document.getElementById('coreContainer')
);
