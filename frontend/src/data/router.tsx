import { LoginForm } from 'component/connexion/loginuser';
import { LogoutForm } from 'component/connexion/logoutuser';
import { RegisterForm } from 'component/connexion/registeruser';
import { Dashboard } from 'component/dashboard/dashboard';
import { BASE_HREF } from 'config.json';
import { UserContext } from 'context/usercontext';
import React from 'react';
import { Route, Switch } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';



interface Props { }
interface State { }

export class Router extends React.Component<Props, State> {
    public static contextType = UserContext;
    public context: UserContext;

    constructor(props: Props) {
        super(props);
    }

    public render() {
        return <BrowserRouter basename={BASE_HREF}>
            <header className='bloc-page right'>
                {this.context.user ? <LogoutForm /> : <LogoutForm />}
                <ToastContainer />
            </header>
            <Switch>
                <Route path='/registeruser' component={RegisterForm} />
                <Route path='/connexion' component={LoginForm} />
                <Route path='/dashboard' component={Dashboard} />
            </Switch>
        </BrowserRouter>;
    }
}
