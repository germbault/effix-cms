import { Api } from 'api';
import { UserContext } from 'context/usercontext';
import React from 'react';
import { Redirect } from 'react-router';

interface Props { }
interface State { }

export class LogoutForm extends React.Component<Props, State> {
    public static contextType = UserContext;
    public context: UserContext;
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        if (!this.context.user) {
            return <Redirect to='/connexion' />;
        }
        return <button className='logout-button' onClick={this.logout} >Se déconnecter</button>;
    }

    private logout = async () => {
        await this.api.post('/auth/logout');
        this.context.setUser(null);
    };
}
