import { Api } from 'api';
import { UserModel } from 'common';
import { UserContext } from 'context/usercontext';
import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

interface Props { }
interface State {
    username?: string;
    first_name?: string;
    last_name?: string;
    email?: string;
    password?: string;
}

export class RegisterForm extends React.Component<Props, State> {
    public static contextType = UserContext;
    public context: UserContext;
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    public render() {
        if (this.context.user === undefined) { return null; }
        if (this.context.user) {
            return <Redirect to='/dashboard' />;
        }
        return <>
            <section className='bloc-page'>
                <section id='connexionForm'>
                    <Link to={`/connexion`}><img src='../../img/programming.png' alt='logo' /></Link>
                    <form className='registerStyle' onSubmit={this.register}>
                        <h2>Créer votre compte</h2>
                        <section className='form'>

                            <input type='text' placeholder='Username' required={true} value={this.state.username ?? ''} onChange={e => { this.setState({ username: e.target.value }); }} />
                            <input type='text' placeholder='Prénom' required={true} value={this.state.first_name ?? ''} onChange={e => { this.setState({ first_name: e.target.value }); }} />
                            <input type='text' placeholder='Nom' required={true} value={this.state.last_name ?? ''} onChange={e => { this.setState({ last_name: e.target.value }); }} />
                            <input type='email' placeholder='Courriel' required={true} value={this.state.email ?? ''} onChange={e => { this.setState({ email: e.target.value }); }} />
                            <input type='password' placeholder='Mot de passe' required={true} value={this.state.password ?? ''} onChange={e => { this.setState({ password: e.target.value }); }} />

                            <input type='submit' value='Envoyé' />
                        </section>
                    </form>
                </section>
            </section>
        </>;
    }

    private register = async (event: React.FormEvent) => {
        event.preventDefault();

        try {
            const user = UserModel.fromJSON(await this.api.postGetJson('/auth/user',
                { username: this.state.username, first_name: this.state.first_name, last_name: this.state.last_name, email: this.state.email, password: this.state.password }));
            this.context.setUser(user);
            toast.success('Bienvenu, votre compte à été crée.');
        } catch {
            toast.error('Votre nom dulisateur est déja utilisé.');
        }
    };
}
