import { Api } from 'api';
import { ApiWp } from 'apiwp';
import { CustomerModel, WpModel } from 'common';
import React from 'react';
import { toast } from 'react-toastify';
// import { UserInfo } from './formuserinfo';


interface Props { }
interface State {
    customers: CustomerModel[];
    wpUsers?: WpModel[];
    user: CustomerModel;
    username?: string;
    userId_wordpress: string;
    userId_wordpress_max: number;

    customer_site?: string;
    text?: string;
    image?: string;
    publicityActive?: string;
}

export class Dashboard extends React.Component<Props, State> {
    private apiWp = new ApiWp;
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {
            customers: [],
            wpUsers: [],
            user: new CustomerModel,
            username: '',
            userId_wordpress: '',
            userId_wordpress_max: 0,
        };
    }

    public async componentDidMount() {

        const customers = (await this.api.getJson('/customer') as any[]).map(CustomerModel.fromJSON);
        this.setState({ customers });

        this.checkIdWordpressMax();
    }

    public render() {
        const { user, wpUsers, customers } = this.state;

        return <>
            <section className='bloc-page'>
                <h1>Tableau de bort</h1>
                { }
                {/* <TestAffichage /> */}
                <section className='new-subscription-section'>
                    <h2>Synchroniser les nouveaux abonnements</h2>
                    <section className='new-subscription-section--button-center'>
                        <button type='button' onClick={() => this.newCustomers()}><img src='../../../img/synchronization-button.png' /></button>
                    </section>
                </section>

                <section className='customer-section'>
                    <section className='col-40'>
                        <h3>Liste de client</h3>
                        <nav>
                            <ul onClick={(e: React.MouseEvent<HTMLUListElement, MouseEvent>) => {
                                const userToEdit = customers.find(u => u.customerId === ((e.target as any).value));
                                this.setState({ user: userToEdit ?? new CustomerModel });
                            }}>
                                {!wpUsers ? 'Chargement...' : customers!.map(post => <li key={post.customerId} id={post.customerId.toString()} value={post.customerId}> {post.username}</li>)}
                            </ul>
                        </nav>
                    </section>

                    {/* <UserInfo user={user} /> */}
                    <section className='col-60'>
                        <h3>Client actuel</h3>
                        {(user.customerId === undefined) ? <legend>Sélectionner un client.</legend>
                            : <>

                                <form onSubmit={this.createdCustomerInfo} encType='multipart/form-data'>
                                    <fieldset>
                                        <legend>Nom du client : {this.state.user.username}</legend>
                                        <p>
                                            <label className=''>Activer / désactiver la publicité!</label><br />
                                            {/* <select value={this.state.publicityActive} onChange={e => { this.setState({ publicityActive: e.target.value }); }} > */}
                                            <select value={this.state.user.publicityActive} onChange={e => {
                                                user.publicityActive = e.currentTarget.value;
                                                this.setState({ user });
                                            }}>

                                                <option value='' disabled={true} />
                                                <option value='activer'>activer</option>
                                                <option value='desactiver'>désactiver</option>
                                            </select>
                                        </p>
                                        <p>
                                            <label>Site web du client :</label>
                                            {/* <input type='website' name='website' id='website' value={this.state.customer_site ??} onChange={e => { this.setState({ e => customer_site: e.target.value  */}
                                            <input type='website' name='website' id='website' value={this.state.user.customer_site ?? ''} onChange={e => {
                                                user.customer_site = e.currentTarget.value;
                                                this.setState({ user });
                                            }} />
                                        </p>
                                        <p>
                                            <label>Ligne de texte :</label>
                                            {/* <input type='text' name='text' id='text' value={this.state.text ?? this.props.user.text} onChange={e => { this.setState({ text: e.target.value }); }} /> */}
                                            <input type='text' name='text' id='text' value={this.state.user.text ?? ''} onChange={e => {
                                                user.text = e.currentTarget.value;
                                                this.setState({ user });
                                            }} />
                                        </p>
                                        <p>
                                            <label>Sélectionner une image :</label>
                                            <input type='file' name='uploadFile' id='uploadFile' value={this.state.image ?? ''} onChange={e => { this.setState({ image: e.target.value }); }} />
                                        </p>
                                    </fieldset>
                                    <input className='button' type='submit' value='Envoyé' />
                                </form>
                            </>
                        }
                    </section>

                </section>
            </section>
        </>;
    }

    private newCustomers = async () => {
        const { userId_wordpress_max } = this.state;

        const wpUsers = (await this.apiWp.getJson('/wp-json/wp/v2/users') as any[]).map(WpModel.fromJSON);
        this.setState({ wpUsers });

        wpUsers!.map(async post => {

            if (post.id > userId_wordpress_max) {
                const sendingForm = { userId_wordpress: post.id, username: post.name };
                const createdSendingForm = CustomerModel.fromJSON(await this.api.postGetJson('/customer', sendingForm));

                this.state.customers!.push(createdSendingForm);
                this.setState({ customers: this.state.customers });
                this.checkIdWordpressMax();
            }
        }

        );

        return toast.success('Fin de la sychronisation.');
    };

    private checkIdWordpressMax = () => {
        const { customers } = this.state;

        const userId_wp_max = Math.max.apply(Math, customers.map(customer => { return customer.userId_wordpress; }));

        this.setState({ userId_wordpress_max: userId_wp_max });
    };

    private createdCustomerInfo = async (event: React.FormEvent) => {
        event.preventDefault();
        const { user } = this.state;

        const sendingForm = { customer_site: user.customer_site, text: user.text, image: user.image, publicityActive: user.publicityActive };
        const createdSendingForm = CustomerModel.fromJSON(await this.api.putGetJson('/customer', user.customerId, sendingForm));

        this.setState({ user: createdSendingForm });
        return toast.success('Votre publicité a bien été entrer, MERCI!');
    };
}
