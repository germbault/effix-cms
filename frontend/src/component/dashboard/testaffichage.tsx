/**
 * Bloc de code réutilisable pour l'affichage sur les 2 one pager React.
 * Modifier et terminer le code pour l'ajuster au site correspondant.
 */

import { Api } from 'api';
import { CustomerModel } from 'common';
import React from 'react';



interface Props { }
interface State {
    customers: CustomerModel[];
}

export class TestAffichage extends React.Component<Props, State> {
    private api = new Api;

    constructor(props: Props) {
        super(props);

        this.state = {
            customers: [],
        };
    }

    public async componentDidMount() {

        const customers = (await this.api.getJson('/customer') as any[]).map(CustomerModel.fromJSON);
        this.setState({ customers });
    }

    public render() {
        const { customers } = this.state;

        return <>
            <section className='bloc-page'>
                <section>
                    <h2>Test d'affichage</h2>
                    {customers.map(customer =>
                        <section key={customer.customerId} className=''>
                            <a href={customer.customer_site} target='blank'><p className=''>Site web du clien: {customer.customer_site}</p></a>
                            <p className=''>Ligne de text: {customer.text}</p>
                            {/* <p className=''>Votre image: <img src={customer.image} alt='img' /></p> */}
                            {/* <p className=''>Votre image: <img src={`/uploads/${img.​img.path}`} alt='img' /></p> */}
                            <p className=''>Votre image: <img src='../../../img/epee02.png' /></p>
                        </section>)}
                </section>
            </section>
        </>;
    }
}
